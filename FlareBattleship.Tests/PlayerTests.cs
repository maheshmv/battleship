using System;
using Xunit;
using FlareBattleship;

namespace FlareBattleship.Tests
{
    
    public class UnitTest1
    {
        IPlayer player1;
        IPlayer player2;

        private void SetupTwoPlayers()
        {
            player1 = new Player();
            player2 = new Player();
            player1.SetOpponent(player2);
            player2.SetOpponent(player1);
        }
        [Fact]
        public void AttacktoWIN()
        {
            SetupTwoPlayers();
            player1.PlaceShipOnBoard(new ShipPosition { X = 0, Y = 0, Length = 3, Align = ShipPosition.AlignmentType.Horizontal });
            player2.PlaceShipOnBoard(new ShipPosition { X = 5, Y = 2, Length = 3, Align = ShipPosition.AlignmentType.Horizontal });

            Assert.True(player1.AttackOpponent(5, 2));
            Assert.True(player1.AttackOpponent(7, 2));
            Assert.False(player2.HasLost());
            Assert.True(player1.AttackOpponent(6, 2));
            Assert.True(player2.HasLost());
        }
        [Fact]
        public void TargetHitMiss()
        {
            SetupTwoPlayers();
            player1.PlaceShipOnBoard(new ShipPosition { X = 7, Y = 1, Length = 3, Align = ShipPosition.AlignmentType.Vertical });
            player2.PlaceShipOnBoard(new ShipPosition { X = 5, Y = 4, Length = 3, Align = ShipPosition.AlignmentType.Vertical });

            Assert.False(player1.AttackOpponent(5, 3));
            Assert.False(player1.AttackOpponent(7, 0));
            Assert.False(player1.AttackOpponent(6, 2));
        }

        [Fact]
        public void AttackOutsideGrid()
        {
            SetupTwoPlayers();
            player1.PlaceShipOnBoard(new ShipPosition { X = 0, Y = 0, Length = 3, Align = ShipPosition.AlignmentType.Horizontal });
            player2.PlaceShipOnBoard(new ShipPosition { X = 5, Y = 2, Length = 3, Align = ShipPosition.AlignmentType.Horizontal });

            Assert.False(player1.AttackOpponent(10, 2));
            Assert.False(player1.AttackOpponent(5, -2));
        }
        [Fact]
        public void PlaceShipOutsideGrid()
        {
            SetupTwoPlayers();
            bool ship1 = player1.PlaceShipOnBoard(new ShipPosition { X = 0, Y = 7, Length = 4, Align = ShipPosition.AlignmentType.Vertical });

            bool ship2 = player2.PlaceShipOnBoard(new ShipPosition { X = 8, Y = 2, Length = 3, Align = ShipPosition.AlignmentType.Horizontal });

            Assert.False(ship1);
            Assert.False(ship2);
        }
        [Fact]
        public void PlaceTwoShipsColliding()
        {
            SetupTwoPlayers();  
            player1.PlaceShipOnBoard(new ShipPosition { X = 6, Y = 2, Length = 4, Align = ShipPosition.AlignmentType.Vertical });
            bool ret = player1.PlaceShipOnBoard(new ShipPosition { X = 3, Y = 5, Length = 5, Align = ShipPosition.AlignmentType.Horizontal });
            Assert.False(ret);
        }
        [Fact]
        public void DoubleHitSameCell()
        {
            SetupTwoPlayers();
            player1.PlaceShipOnBoard(new ShipPosition { X = 0, Y = 0, Length = 3, Align = ShipPosition.AlignmentType.Horizontal });
            player2.PlaceShipOnBoard(new ShipPosition { X = 5, Y = 2, Length = 3, Align = ShipPosition.AlignmentType.Horizontal });

            Assert.True(player1.AttackOpponent(5, 2));
            Assert.False(player1.AttackOpponent(5, 2));
        }
        [Fact]
        public void TestReceiveAttack()
        {
            SetupTwoPlayers();
            player1.PlaceShipOnBoard(new ShipPosition { X = 8, Y = 0, Length = 3, Align = ShipPosition.AlignmentType.Vertical });
            player2.PlaceShipOnBoard(new ShipPosition { X = 5, Y = 2, Length = 3, Align = ShipPosition.AlignmentType.Horizontal });

            Assert.True(player1.ReceiveAttack(8, 2));
            Assert.True(player2.ReceiveAttack(7, 2));
        }
        [Fact]
        public void AddLargeShip()
        {
            SetupTwoPlayers();
            bool ret = player1.PlaceShipOnBoard(new ShipPosition { X = 0, Y = 0, Length = 10, Align = ShipPosition.AlignmentType.Vertical });
            Assert.True(ret);

            ret = player2.PlaceShipOnBoard(new ShipPosition { X = 0, Y = 0, Length = 10, Align = ShipPosition.AlignmentType.Horizontal });
            Assert.True(ret);

            SetupTwoPlayers();
            ret = player1.PlaceShipOnBoard(new ShipPosition { X = 1, Y = 0, Length = 10, Align = ShipPosition.AlignmentType.Horizontal });
            Assert.False(ret);

            ret = player1.PlaceShipOnBoard(new ShipPosition { X = 0, Y = 0, Length = 11, Align = ShipPosition.AlignmentType.Horizontal });
            Assert.False(ret);
        }
        [Fact]
        public void AttackWithNoOpponent()
        {
            SetupTwoPlayers();
            player1.PlaceShipOnBoard(new ShipPosition { X = 8, Y = 0, Length = 3, Align = ShipPosition.AlignmentType.Vertical });
            
            player1.SetOpponent(null);
           
            Assert.False(player1.AttackOpponent(8, 0));
           
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlareBattleship
{
    class BattleshipAdmin
    {
        public const int NumOfPlayers = 2;
        public static IPlayer[] InitializePlayers()
        {
            IPlayer[] player = new Player[NumOfPlayers];
            for(int i =0; i<player.Length;i++)
            {
                player[i] = new Player();
            }
            player[0].SetOpponent(player[1]);
            player[1].SetOpponent(player[0]);
            
            return player;
        }

    }
}

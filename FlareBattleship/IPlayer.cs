﻿namespace FlareBattleship
{
    public interface IPlayer
    {
        //void CreateBoard();
        bool ReceiveAttack(int x, int y);// Receive the hit from the opponent
        bool AttackOpponent(int x, int y);  // Hit the opponent
        bool HasLost();
        void SetOpponent(IPlayer p);
        bool PlaceShipOnBoard(ShipPosition pos);
    }
}

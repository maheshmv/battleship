﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlareBattleship
{

    public struct Cell {
        public int X { get; set; }
        public int Y { get; set; }
    }
    public class Player : IPlayer
    {
        public int[,] Board;
        public int[,] OpponentGrid;
        IPlayer opponent;
        
        private List<List<Cell>> Ships = new List<List<Cell>>();
        public Player()
        {
            Board = new int[10, 10];
            OpponentGrid = new int[10, 10];
        }

        public bool ReceiveAttack(int x, int y)
        {
            if (x < 0 || y < 0 || x > 9 || y > 9 || Ships?.Count==0)
                return false;

            if (Board[x, y] != 1)
                return false;
            Cell cell = new Cell { X = x, Y = y };

            bool ret = false;
            foreach(var ship in  Ships)
            {
                if(ship.Contains(cell))
                {
                    ship.Remove(cell);
                    Board[x, y] = 0;
                    if(ship.Count==0)
                    {
                        Ships.Remove(ship); // Ship sinks
                    }
                    ret = true;
                    break;
                }
            }
            return ret;
        }
        public bool HasLost()
        {
            if (Ships.Count == 0)
                return true;
            else
                return false;
        }
        
        public bool AttackOpponent(int x, int y)
        {
            if (x < 0 || y < 0 || x > 9 || y > 9)
                return false;
            if (opponent == null || OpponentGrid[x,y]==1) // already hit at (x,y)
                return false;

            bool Hit = opponent.ReceiveAttack(x, y);
            if(Hit)
            {
                OpponentGrid[x, y] = 1;
                Console.WriteLine("HIT");
            }
            else
            {
                Console.WriteLine("MISS");
            }
            return Hit;
        }

        public bool PlaceShipOnBoard(ShipPosition pos)
        {
            if (pos.Length > 10 || 
                    pos.X < 0 || pos.Y < 0 || 
                    pos.X >= 10 || pos.Y >= 10)
                return false;

            int dx = 0, dy = 0;
            List<Cell> ship = new List<Cell>();
            if (pos.Align == ShipPosition.AlignmentType.Horizontal)
                dx = 1;
            else
                dy = 1;

            if (pos.X + (pos.Length - 1) * dx >= 10 ||
                pos.Y + (pos.Length - 1) * dy >= 10)  // ship going out of boundaries
            {
                return false;
            }

            for (int i = 0; i < pos.Length && i < 10; i++)
            {
                int NextX = pos.X + i * dx;
                int NextY = pos.Y + i * dy;

            // check if possible to set otherwise return false;
                if (Board[NextX, NextY] != 1)
                {
                    Board[NextX, NextY] = 1;
                    ship.Add(new Cell { X = NextX, Y = NextY });
                }
                else // Can't place on the pre-occupied cell.
                {
                    // Start rollback the board cells set to 1
                    while(!(NextX==pos.X&&NextY==pos.Y))
                    {
                        NextX = NextX -  dx;
                        NextY = NextY -  dy;
                        Board[NextX, NextY] = 0;
                    }
                    ship.Clear();
                    return false;
                }
            }

            Ships.Add(ship);
            return true;
        }

        public void SetOpponent(IPlayer p)
        {
            if (this == p)
                return;
            opponent = p;
        }
    }
}

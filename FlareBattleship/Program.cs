﻿using Align = FlareBattleship.ShipPosition.AlignmentType;

namespace FlareBattleship
{
    class Program
    {
        static void Main(string[] args)
        {
            //BattleshipAdmin.

            bool lostBattle = false;
            IPlayer[] p = BattleshipAdmin.InitializePlayers();
            IPlayer p1 = p[0];
            IPlayer p2 = p[1];
            ShipPosition pos = new ShipPosition { X = 0, Y = 3, Length = 4, Align = Align.Horizontal };
            p1.PlaceShipOnBoard(pos);
            pos = new ShipPosition { X = 3, Y = 0, Length = 4, Align = Align.Vertical };
            p1.PlaceShipOnBoard(pos);
            pos = new ShipPosition { X = 5, Y = 5, Length = 5, Align = Align.Vertical };
            p1.PlaceShipOnBoard(pos);
            pos = new ShipPosition { X = 3, Y = 0, Length = 5, Align = Align.Horizontal };
            p1.PlaceShipOnBoard(pos);

            pos = new ShipPosition { X = 4, Y = 5, Length = 4, Align =Align.Horizontal };
            p2.PlaceShipOnBoard(pos);
            pos = new ShipPosition { X = 7, Y = 2, Length = 4, Align = Align.Horizontal };
            p2.PlaceShipOnBoard(pos);
            pos = new ShipPosition { X = 7, Y = 2, Length = 3, Align = Align.Horizontal };
            p2.PlaceShipOnBoard(pos);
            pos = new ShipPosition { X = 1, Y = 6, Length = 4, Align = Align.Vertical };
            p2.PlaceShipOnBoard(pos);

            p1.AttackOpponent(2, 5);
            p2.AttackOpponent(5, 7);

            p1.AttackOpponent(9, 2);
            p2.AttackOpponent(5, 9);

            p1.AttackOpponent(3, 3);
            p2.AttackOpponent(5, 8);

            p1.AttackOpponent(8, 2);
            p2.AttackOpponent(1, 3);

            p1.AttackOpponent(5, 5);
            p2.AttackOpponent(8, 9);

            p1.AttackOpponent(6, 5);
            p2.AttackOpponent(3, 3);

            p1.AttackOpponent(6, 5);
            p1.AttackOpponent(7, 5);
            p2.AttackOpponent(5, 5);

            p1.AttackOpponent(8, 5);
            p2.AttackOpponent(5, 6);

            p1.AttackOpponent(4, 5);
            p2.AttackOpponent(3, 2);

            p1.AttackOpponent(7, 2);
            p2.AttackOpponent(3, 3);

            p1.AttackOpponent(1, 7);
            p2.AttackOpponent(3, 4);

            p1.AttackOpponent(1, 5);
            lostBattle = p2.HasLost();
            p2.AttackOpponent(0, 3);
            lostBattle = p1.HasLost();

            p1.AttackOpponent(2, 5);
            lostBattle = p2.HasLost();
            p2.AttackOpponent(2, 3);
            lostBattle = p1.HasLost();

        }
    }
}
